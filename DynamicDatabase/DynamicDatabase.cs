﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Reflection;

namespace DynamicDatabase
{
    public class DynamicDatabase
    {
        private string _connectionString;
        public bool IsCaseSensitive { get; set; }

        public DynamicDatabase(string connectionString)
        {
            _connectionString = connectionString;
            IsCaseSensitive = true;
        }

        public List<T> GetListFromSproc<T>(string procedureName, dynamic param = null)
        {
            SqlConnection conn = null;
            SqlDataReader rdr = null;

            try
            {
                conn = new SqlConnection(_connectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand(procedureName, conn) { CommandType = CommandType.StoredProcedure };

                // Parameter aus dynamic lesen und als cmdParameters einfügen
                PropertyInfo[] sqlParameter = param?.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);

                if (sqlParameter != null)
                {
                    foreach (var propertyInfo in sqlParameter)
                    {
                        cmd.Parameters.Add(new SqlParameter($@"{propertyInfo.Name}", param.GetType().GetProperty(propertyInfo.Name).GetValue(param, null)));
                    }
                }

                // Ausführen
                rdr = cmd.ExecuteReader();

                var rows = new List<T>();
                while (rdr.Read())
                {
                    //Für jede Zeile ein neues dynamisches Objekt erzeugen => PropertyNames = Spaltennamen
                    var row = new ExpandoObject() as IDictionary<string, object>;
                    for (int i = 0; i < rdr.FieldCount; i++)
                    {
                        row.Add(rdr.GetName(i), rdr[i]);
                    }

                    // die Properties aus dem übergebenen Typ lesen
                    var props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(x => x.GetSetMethod() != null);
                    var obj = Activator.CreateInstance(typeof(T));

                    // wenn es ein Property im Ergebnis und im Typ gibt, dann zuweisen
                    foreach (var prop in props)
                    {
                        if (row.ContainsKey(prop.Name))
                        {
                            prop.SetValue(obj, row[prop.Name]);
                        }
                    }

                    //zur Ergebnisliste des übergebenen Typs hinzufügen
                    rows.Add((T)obj);
                }

                return rows;
            }
            finally
            {
                conn?.Close();
                rdr?.Close();
            }
        }
    }
}
