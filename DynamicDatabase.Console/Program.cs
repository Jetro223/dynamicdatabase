﻿using System;

namespace DynamicDatabase.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var r = new DynamicDatabase("Server=localhost;Database=DynamicDatabaseTest;User Id=fatigue;Password=FIglKXmPz3dY6lYeIxrx");
            r.IsCaseSensitive = false;
            var dataList = r.GetListFromSproc<DataClass>("[sproc_Test_Dynamic]"
                , new
                {
                    para_string = "Mein String",
                    para_int = 999,
                    para_date = DateTime.Now.AddMinutes(-10),
                    para_bit = true,
                    para_decimal = 12.555
                });

            foreach (var data in dataList)
            {
                System.Console.WriteLine(data.ToString());
            }

            System.Console.ReadLine();
        }
    }
}



/* Prozedur

CREATE PROCEDURE [sproc_Test_Dynamic] (
         @para_string VARCHAR(38),
		 @para_int INT,
		 @para_date DATETIME,
		 @para_bit BIT,
		 @para_decimal DECIMAL(10,6)
	  )
AS
BEGIN


	SELECT	 IntValue	  = 1234
			,StringValue  = 'My string'
			,DateValue	  = GETDATE()
			,BitValue	  = CAST(1 AS BIT)
			,DecimalValue = 12.5558			
	UNION
	SELECT	 IntValue	  = @para_int
			,StringValue  = @para_string
			,DateValue	  = @para_date
			,BitValue	  = @para_bit
			,DecimalValue = @para_decimal

END
 */
